import 'package:flutter/material.dart';
import 'package:ma_carte_de_visite/ressources/cont_global.dart';

class Details extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(colorBackground),
      appBar: AppBar(
        title: Text("En savoir plus"),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Center(
        child: ListView(
          padding: EdgeInsets.all(8.0),
          shrinkWrap: true,
          children: [
            Container(
              alignment: Alignment.center,
              child: CircleAvatar(
                radius: 70.0,
                backgroundImage: AssetImage("assets/David.jpg"),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Card(
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  descripCarteDeVisite,
                  textAlign: TextAlign.center,
                  style: styleDescripCarteDeVisite,
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.mail,
                  color: Colors.white,
                  size: 35.0,
                ),
                SizedBox(
                  width: 15.0,
                ),
                Text(
                  emailCarteDeVisite,
                  style: styleEmailCarteDeVisite,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
