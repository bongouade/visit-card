import 'package:flutter/material.dart';

import 'details.dart';

import '../ressources/cont_global.dart';

class VisitCard extends StatelessWidget {
  @override
  Widget build(context) {
    return Scaffold(
      backgroundColor: Color(colorBackground),
      appBar: AppBar(
        title: Text("Ma carte de visite"),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 70.0,
              backgroundImage: AssetImage("assets/David.jpg"),
            ),
            SizedBox(
              height: 10,
            ),
            Card(
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  nomCarteDeVisite,
                  style: styleNomCarteDeVisite,
                ),
              ),
            ),
            Card(
              color: Colors.transparent,
              margin: EdgeInsets.only(top: 30.0, bottom: 15.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  statutcarteDeVisite,
                  textAlign: TextAlign.center,
                  style: styleStatuCarteDeVisite,
                ),
              ),
            ),
            RaisedButton(
              child: Text(
                textbuttonCarteDeVisite,
                style: styleButtonCartedeVisite,
              ),
              color: Colors.blueGrey,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (BuildContext context) {
                    return Details();
                  }),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
